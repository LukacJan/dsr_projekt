<?php //session_start(); 
include_once('php/povezava.php')
?>
<html>

<head>
    <meta charset="UTF-8">
    <!-- <script src="script/prijava.js"></script> -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" />
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <!--<script src="script/nav_bar.js"></script>-->
</head>

<body>
    <nav id="nav" class="navbar navbar-expand-lg navbar-light bg-light">
    <?php
    include("php/nav_bar.php");
    ?>
    </nav>

    <form method="post" onsubmit="">
        <div class="form-group">
            <label for="inputMail">Email</label>
            <input type="email" class="form-control" id="inputMail" name="email" aria-describedby="emailHelp" placeholder="Vnesi email">
        </div>
        <br/>
        <div class="form-group">
            <label for="inputGeslo">Password</label>
            <input type="password" class="form-control" id="inputGeslo" name="geslo" placeholder="Geslo">
        </div>
        <br/>
        <button type="submit" class="btn btn-primary">Prijava</button>
    </form>
    <?php include 'php/prijava.php'; ?>
</body>

</html>