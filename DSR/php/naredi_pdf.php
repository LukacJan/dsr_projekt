<?php
require('../fpdf/fpdf.php');

class PDF extends FPDF
{

    // Page header
    function Header()
    {


        // Set font family to Arial bold 
        $this->SetFont('Arial', 'B', 20);

        // Move to the right
        $this->Cell(60);

        // Header
        $this->Cell(60, 10, 'Podatki vremena', 1, 0, 'C');

        // Line break
        $this->Ln(20);
    }

    // Page footer
    function Footer()
    {

        // Position at 1.5 cm from bottom
        $this->SetY(-15);

        // Arial italic 8
        $this->SetFont('Arial', 'I', 8);

        // Page number
        $this->Cell(0, 10, 'Stran ' .
            $this->PageNo() . '/{nb}', 0, 0, 'C');
    }
}

$ip;
if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip = $_SERVER['HTTP_CLIENT_IP'];
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
    $ip = $_SERVER['REMOTE_ADDR'];
}

if ($ip == "::1") {
    $location = file_get_contents('http://api.ipstack.com/164.8.243.222?access_key=0615d7aff3db9f45f605446f95a33593&format=1');
    $obj = json_decode($location);
    
    $city = $obj->{'city'};
} else {
    $location = file_get_contents('http://api.ipstack.com/' . $ip . '?access_key=0615d7aff3db9f45f605446f95a33593&format=1');
    $obj = json_decode($location);
    
    $city = $obj->{'city'};
}

$vreme = file_get_contents('https://api.openweathermap.org/data/2.5/weather?q=' . $city . '&appid=6b919022ce964ddd09e483b14a614d69&units=metric&lang=sl');
//print $vreme;

//var_dump($vreme);
$vreme = json_decode($vreme);

// Instantiation of FPDF class
$pdf = new PDF();



// Define alias for number of pages
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times', '', 14);

$min_temp ='Minimalna dnevna temperatura: ' . $vreme->main->temp_min . '°C';
$temp = 'Trenutna temperatura: ' . $vreme->main->temp . '°C';
$obc_temp = 'Obcutek temperature: ' . $vreme->main->feels_like . '°C';
$max_temp = 'Maksimalna dnevna temperatura: ' . $vreme->main->temp_max . '°C';
$vlaga = 'Odstotek vlage: ' . $vreme->main->humidity . '%';
$veter = 'Hitrost vetra: ' . $vreme->wind->speed . 'm/s';
$oblaki = 'Pokritost neba z oblaki : ' . $vreme->clouds->all . '%';

$pdf->Cell(0, 10, 'V kraju ' . $vreme->name . 'je vreme naslednje:', 0, 1);
$pdf->Cell(0, 10, 'Opis vremena: ' . iconv('UTF-8', 'windows-1252', $vreme->weather[0]->description), 0, 1);
$pdf->Cell(0, 10, iconv('UTF-8', 'windows-1252', $temp), 0, 1);
$pdf->Cell(0, 10, iconv('UTF-8', 'windows-1252', $obc_temp), 0, 1);
$pdf->Cell(0, 10, iconv('UTF-8', 'windows-1252', $min_temp), 0, 1);
$pdf->Cell(0, 10, iconv('UTF-8', 'windows-1252', $max_temp), 0, 1);
$pdf->Cell(0, 10, iconv('UTF-8', 'windows-1252', $vlaga), 0, 1);
$pdf->Cell(0, 10, iconv('UTF-8', 'windows-1252', $veter), 0, 1);
$pdf->Cell(0, 10, iconv('UTF-8', 'windows-1252', $veter), 0, 1);

$pdf->Output();
