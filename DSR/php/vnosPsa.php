<?php
mb_internal_encoding("UTF-8");
include_once("povezava.php");
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    //dobi podatke psa vnesene v form preko post metode
    $ime = $_POST['ime'];
    $ime = ($ime);
    $teza = $_POST['teza'];
    $leto = $_POST['leto'];
    $uporabnikID = $_SESSION['prijavljen_id'];
    $spol = $_POST['spol'];
    $aktivnost = $_POST['aktivnost'];
    $pasma = $_POST['pasma'];
    echo "<br />";
    print_r($_FILES);

    try {
        $sql = "INSERT INTO pes (ime, teza, leto_rojstva, TK_ID_uporabnik, TK_ID_spol, TK_ID_aktivnost, TK_ID_pasma) VALUES (?,?,?,?,?,?,?)";
        $stmt = $conn->prepare($sql);
        $stmt->execute([$ime, $teza, $leto, $uporabnikID, $spol, $aktivnost, $pasma]);
        $id = $conn->lastInsertId();
        echo $id;

        if (file_exists($_FILES['files']['tmp_name']) || is_uploaded_file($_FILES['files']['tmp_name'])) {

            $target_dir = "slike/";
            $temp = explode(".", $_FILES["files"]["name"]);
            $newfilename = round(microtime(true)) . '.' . end($temp);

            $target_file = $target_dir . $newfilename;
            $uploadOk = 1;
            $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

            if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
                echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $uploadOk = 0;
            }

            if ($uploadOk == 0) {
                echo "Sorry, your file was not uploaded.";
                // if everything is ok, try to upload file
            } else {
                if (move_uploaded_file($_FILES["files"]["tmp_name"], $target_file)) {

                    $sql2 = "INSERT INTO slika (naziv, TK_ID_pes) VALUES (?,?)";
                    $stmt2 = $conn->prepare($sql2);
                    $stmt2->execute([$newfilename, $id]);
                    /*
                    echo "The file " . htmlspecialchars(basename($_FILES["files"]["name"])) . " has been uploaded.";

                    echo "<br />";
                    echo $newfilename;
                    echo "<br />";
                    echo $newfilename;*/
                } else {
                    echo "Sorry, there was an error uploading your file.";
                }
            }
        }




        echo "<script>console.log('New record created successfully');</script>";
        echo '<div class="alert alert-success alert-dismissible fade show" role="alert">
               Vnešen je bil nov pes.
               <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>';
        //header("Location: index.php");
    } catch (PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
        echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
               Pri vnosu je prišlo do napake.
               <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>';
    }
}
