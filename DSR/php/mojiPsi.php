<?php
mb_internal_encoding("UTF-8");
include_once("povezava.php");

try {
    $stmt = $conn->prepare('SELECT *  FROM pes WHERE TK_ID_uporabnik = ?');
    $stmt->execute(array($_SESSION['prijavljen_id']));
    $odg = $stmt->fetchAll();

    //print_r($odg);

    for ($i=0; $i < count($odg); $i++) { 
        $id = $odg[$i][0];

        $stmt2 = $conn->prepare('SELECT naziv  FROM slika WHERE TK_ID_pes = ? ');
        $stmt2->execute(array($id));
        $odg2 = $stmt2->fetchColumn();

        if ($i % 2 == 0) {
            echo '<div class="row my-2 mx-auto">';
        }

        $spol;

        if ($odg[$i]['TK_ID_spol'] == 1) {
            $spol = "Moški";
        }else {
            $spol = "Ženska";
        }

        $stmt3 = $conn->prepare('SELECT naziv  FROM aktivnost WHERE id_aktivnost = ? ');
        $stmt3->execute(array($odg[$i]['TK_ID_pasma']));
        $aktivnost = $stmt3->fetchColumn();

        $stmt4 = $conn->prepare('SELECT naziv  FROM pasma WHERE id_pasma = ? ');
        $stmt4->execute(array($odg[$i]['TK_ID_aktivnost']));
        $pasma = $stmt4->fetchColumn();

        if ($odg2 > 0 && $odg2 != '') {
            echo '<div class="col-5 mx-auto border bg-info border-info rounded-3 bg-opacity-10 row">
            <div class="col">
                <h5>Ime: '. $odg[$i][1] . '</h5>
                <h6>Teža: '. $odg[$i]['teza'] . '</h6>
                <h6>Leto rojstva: '. $odg[$i]['leto_rojstva'] . '</h6>
                <h6>Spol: '. $spol . '</h6>
                <h6>Pasma: '. $pasma . '</h6>
                <h6>Aktivnost: '. $aktivnost . '</h6>
            </div>
            <div class="col">
            <img src="slike/'. $odg2 .'" class="rounded mx-1 img-thumbnail my-1 d-block img-fluid" alt="..." />
            </div>
            <form method="post" action="php/izbrisiPsa.php">
            <button type="submit" name="id" class="homebutton btn btn-danger" value="' . $odg[$i][0] . '">Izbriši psa</button>
            </form>
        </div>';
        }else {
            $standard = "standard.jpg";
            echo '<div class="col-5 mx-auto border bg-info border-info rounded-3 bg-opacity-10 row">
            <div class="col">
                <h5>Ime: '. $odg[$i][1] . '</h5>
                <h6>Teža: '. $odg[$i]['teza'] . '</h6>
                <h6>Leto rojstva: '. $odg[$i]['leto_rojstva'] . '</h6>
                <h6>Spol: '. $spol . '</h6>
                <h6>Pasma: '. $pasma . '</h6>
                <h6>Aktivnost: '. $aktivnost . '</h6>
            </div>
            <div class="col">
            <img src="slike/'. $standard .'" class="rounded mx-1 img-thumbnail my-1 d-block img-fluid" alt="..." />
            </div>
            <form method="post" action="php/izbrisiPsa.php">
            <button type="submit" name="id" class="homebutton btn btn-danger" value="' . $odg[$i][0] . '">Izbriši psa</button>
            </form>
        </div>';
        }

        if ($i % 2 == 1) {
            echo '</div>';
        }
    }
} catch (\Throwable $th) {
    //throw $th;
}

?>