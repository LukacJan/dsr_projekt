<?php
mb_internal_encoding("UTF-8");
include_once("povezava.php");

try {
    $stmt = $conn->prepare('SELECT *  FROM pot WHERE TK_ID_uporabnik = ?');
    $stmt->execute(array($_SESSION['prijavljen_id']));
    $odg = $stmt->fetchAll();

    for ($i = 0; $i < count($odg); $i++) {

        $id = $odg[$i][0];


        $stmt12312312 = $conn->prepare('SELECT ocena  FROM ocena WHERE TK_ID_pot = ? ');
        $stmt12312312->execute(array($id));
        $ocene = $stmt12312312->fetchAll();

        $ocena = 0;

        if (!empty($ocene)) {
            for ($j = 0; $j < count($ocene); $j++) {
                $ocena += $ocene[$j]['ocena'];
            }

            $ocena = round($ocena / count($ocene), 1);
        } else {
            $ocena = 'Ni še nobene ocene.';
        }

        $stmt2 = $conn->prepare('SELECT naziv  FROM slika WHERE TK_ID_pot = ? ');
        $stmt2->execute(array($id));
        $odg2 = $stmt2->fetchColumn();

        if ($i % 2 == 0) {
            echo '<div class="row my-2 mx-auto">';
        }

        $stmt3 = $conn->prepare('SELECT naziv  FROM podlaga WHERE id_podlaga = ? ');
        $stmt3->execute(array($odg[$i]['TK_ID_podlaga']));
        $podlaga = $stmt3->fetchColumn();

        $stmt4 = $conn->prepare('SELECT ime  FROM pes WHERE id_pes = ? ');
        $stmt4->execute(array($odg[$i]['TK_ID_pes']));
        $pes = $stmt4->fetchColumn();

        $stmt4 = $conn->prepare('SELECT ime  FROM uporabnik WHERE id_uporabnik = ? ');
        $stmt4->execute(array($odg[$i]['TK_ID_uporabnik']));
        $uporabnik = $stmt4->fetchColumn();



        if (isset($_SESSION['prijavljen_id'])) {
            if ($odg2 > 0 && $odg2 != '') {
                echo '<div class="col-5 mx-auto border bg-success border-success rounded-3 bg-opacity-10 row">
                <div class="col">
                    <h5>Naziv: ' . $odg[$i][1] . '</h5>
                    <h6>Dolžina: ' . $odg[$i]['dolzina'] . ' metrov</h6>
                    <h6>Trajanje: ' . $odg[$i]['trajanje'] . ' minut</h6>
                    <h6>Podlaga: ' . $podlaga . '</h6>
                    <h6>Pes: ' . $pes . '</h6>
                    <h6>Lastnik: ' . $uporabnik . '</h6>
                    <h6>Ocena: ' . $ocena . '</h6>
                </div>
                <div class="col">
                <img src="slike/' . $odg2 . '" class="rounded mx-1 img-thumbnail my-1 d-block img-fluid" alt="..." />
                </div>
                <div class="row">
                <form method="post" class="col" action="php/izpisPoti_ocene_catch.php">
                <button type="submit" name="id" class="homebutton btn btn-info" value="' . $odg[$i][0] . '">Ogled ocen in komentarjev</button>
                </form>
                
                <form method="post" class="col" action="php/izbrisiPot.php">
                <button type="submit" name="id" class="homebutton btn btn-danger" value="' . $odg[$i][0] . '">Izbriši pot</button>
                </form>
                </div>
            </div>';
            } else {
                $standard = "sprehod.png";
                echo '<div class="col-5 mx-auto border bg-success border-success rounded-3 bg-opacity-10 row">
                <div class="col">
                    <h5>Naziv: ' . $odg[$i][1] . '</h5>
                    <h6>Dolžina: ' . $odg[$i]['dolzina'] . ' metrov</h6>
                    <h6>Trajanje: ' . $odg[$i]['trajanje'] . ' minut</h6>
                    <h6>Podlaga: ' . $podlaga . '</h6>
                    <h6>Pes: ' . $pes . '</h6>
                    <h6>Lastnik: ' . $uporabnik . '</h6>
                    <h6>Ocena: ' . $ocena . '</h6>
                </div>
                <div class="col">
                <img src="slike/' . $standard . '" class="rounded mx-1 img-thumbnail my-1 d-block img-fluid" alt="..." />
                </div>
                <div class="row">
                <form method="post" class="col" action="php/izpisPoti_ocene_catch.php">
                <button type="submit" name="id" class="homebutton btn btn-info" value="' . $odg[$i][0] . '">Ogled ocen in komentarjev</button>
                </form>
                
                <form method="post" class="col" action="php/izbrisiPot.php">
                <button type="submit" name="id" class="homebutton btn btn-danger" value="' . $odg[$i][0] . '">Izbriši pot</button>
                </form>
                </div>
            </div>';
            }
        } else {
            echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
               Žal nimaš dostopa do te strani.
               <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>';
        }

        if ($i % 2 == 1) {
            echo '</div>';
        }
    }
} catch (\Throwable $th) {
    //throw $th;
}
