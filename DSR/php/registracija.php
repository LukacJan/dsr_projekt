<?php
mb_internal_encoding("UTF-8");
include_once("povezava.php");
//session_start();
if($_SERVER["REQUEST_METHOD"] == "POST") {
    // username and password sent from form 
    

    //dobi email in geslo iz forme poslano po post metodi
    $ime = $_POST['ime'];
    $ime = ($ime);
    $priimek = $_POST['priimek'];
    $priimek = ($priimek);
    $spol = $_POST['spol']; 
    $email = $_POST['email'];
    $geslo = $_POST['geslo'];  

    echo $ime."<br/>".$priimek;

    $sol = random_int(0, 100000000);

    //dobimo sol uporabnika z tem mailom
    $prvo = $conn->prepare('SELECT id_uporabnik  FROM uporabnik WHERE email = ?');
    $prvo->execute(array($email));
    $vrnjen_id = $prvo->fetchColumn();

    //heširamo vpisano geslo skupaj s soljo
    $hashGeslo = hash('ripemd160', $geslo . $sol);

    if (preg_match('/^\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$/', $geslo)) {
        
    }else {
        echo'<div class="alert alert-danger alert-dismissible fade show" role="alert">
        Pri gesla vnosu je prišlo do napake. Vnesite geslo s številko, majhno in veliko črko, posebnim znakom in naj bo dolgo 8 znakov
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
     </div>';
        exit();
    }

    if (empty($vrnjen_id)) {
        try {
            $sql = "INSERT INTO uporabnik (ime, priimek, TK_ID_spol, email, geslo, sol, TK_ID_vloga) VALUES (?,?,?,?,?,?,?)";
            $stmt= $conn->prepare($sql);
            $stmt->execute([$ime, $priimek, $spol, $email, $hashGeslo, $sol, 1]);
            echo "<script>console.log('New record created successfully');</script>";

            $statement = $conn->prepare('SELECT id_uporabnik  FROM uporabnik WHERE email = ? AND geslo = ?');
            $statement->execute(array($email, $hashGeslo));
            $vrnjen_id = $statement->fetchColumn();

            $_SESSION['prijavljen_id'] = $vrnjen_id;

            echo '<script> sessionStorage.setItem("prijavljenID", "' . $_SESSION['prijavljen_id'] . '");</script>';
            header("Location: index.php");
        } catch(PDOException $e) {
            echo $sql . "<br>" . $e->getMessage();
        }
        
    } else {
        echo "<script>console.log('Prišlo je do napake');</script>";
    }
 }
?>