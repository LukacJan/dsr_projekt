<?php
mb_internal_encoding("UTF-8");
include_once("povezava.php");

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // username and password sent from form 


    //dobi email in geslo iz forme poslano po post metodi
    $ocena = $_POST['ocena'];
    $komentar = $_POST['komentar'];

    $id = $_SESSION['oceniPot_id'];

    try {
        $sql = "INSERT INTO ocena (ocena, komentar, TK_ID_pot, TK_ID_uporabnik) VALUES (?,?,?,?)";
        $stmt = $conn->prepare($sql);
        $stmt->execute([$ocena, $komentar, $id, $_SESSION['prijavljen_id']]);
        echo "<script>console.log('New record created successfully');</script>";

        header("Location: index.php");
        exit();
    } catch (\Throwable $th) {
        //throw $th;
    }
}
