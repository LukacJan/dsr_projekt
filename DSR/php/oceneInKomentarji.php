<?php
mb_internal_encoding("UTF-8");
include_once("povezava.php");

$id = $_SESSION['id_Pot_ocene'];

try {

    $stmt = $conn->prepare('SELECT *  FROM ocena WHERE TK_ID_pot  = ?');
    $stmt->execute(array($id));
    $odg = $stmt->fetchAll();

    for ($i = 0; $i < count($odg); $i++) {

        $stmt1 = $conn->prepare('SELECT ime  FROM uporabnik WHERE id_uporabnik  = ?');
        $stmt1->execute(array($odg[$i]['TK_ID_uporabnik']));
        $ime = $stmt1->fetchColumn();

        echo '<div class="row my-2 mx-auto bg-light">
        <h5>Ocena: ' . $odg[$i][1] . '</h5>
        <h6>Komentar: ' . $odg[$i]['komentar'] . '</h6>
        <p>Uporabnik: '. $ime .'</p>
        </div>';
    }
/*
    print_r($odg);
    echo "<br/>";
    print_r(count($odg));*/
} catch (\Throwable $th) {
    //throw $th;
}
