<?php
mb_internal_encoding("UTF-8");
$servername = "localhost";
$username = "root";
$password = "";
$ime_pb = "stran_za_lastnike_psov";

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
if (session_status() === PHP_SESSION_NONE) {
  session_start();
}

try {
  $conn = new PDO("mysql:host=$servername;dbname=$ime_pb;charset=utf8", $username, $password);
  // set the PDO error mode to exception
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  //echo "Connected successfully";
} catch(PDOException $e) {
  echo "Connection failed: " . $e->getMessage();
}
?>