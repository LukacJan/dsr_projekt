<?php
mb_internal_encoding("UTF-8");
include_once("povezava.php");

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $id = $_POST['id'];
    try {
        $sql = "DELETE FROM pes WHERE id_pes = ?";
        $stmt = $conn->prepare($sql);
        $stmt->execute([$id]);
        
        header("Location: ../mojiPsi.php");
    } catch (\Throwable $th) {
        //throw $th;
    }
}
