<?php
if (isset($_SESSION['prijavljen_id'])) {
    echo '<div class="container-fluid">' .
    '<a class="navbar-brand" href="index.php">Psi In Sprehodi</a>' .
    '<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">' .
    '<span class="navbar-toggler-icon"></span>' .
    '</button>' .
    '<div class="collapse navbar-collapse" id="navbarSupportedContent">' .
    '<ul class="navbar-nav me-auto mb-2 mb-lg-0">' .
    '<li class="nav-item">' .
    '<a class="nav-link active" aria-current="page" href="index.php">Domov</a>' .
    '</li>' .
    '<li class="nav-item">' .
    '<a class="nav-link" href="izpisPsov.php">Seznam psov</a>' .
    '</li>' .
    '<li class="nav-item">' .
    '<a class="nav-link" href="izpisPoti.php">Seznam poti</a>' .
    '</li>' .
    '<li class="nav-item">' .
    '<a class="nav-link" href="vnosPsa.php">Vnesi psa</a>' .
    '</li>' .
    '<li class="nav-item">' .
    '<a class="nav-link" href="vnosPoti.php">Vnesi pot</a>' .
    '</li>' .
    '<li class="nav-item">' .
    '<a class="nav-link" href="mojiPsi.php">Moji psi</a>' .
    '</li>' .
    '<li class="nav-item">' .
    '<a class="nav-link" href="mojePoti.php">Moje poti</a>' .
    '</li>' .
    '</ul>' .
    '<form class="d-flex ms-2" action="php/odjava.php">' .
    '<button class="btn btn-outline-danger">Odjava</button>' .
    '</form>' .
    '</div>' .
    '</div>';
}else {
    echo '<div class="container-fluid">' .
    '<a class="navbar-brand" href="index.php">Psi In Sprehodi</a>' .
    '<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">' .
    '<span class="navbar-toggler-icon"></span>' .
    '</button>' .
    '<div class="collapse navbar-collapse" id="navbarSupportedContent">' .
    '<ul class="navbar-nav me-auto mb-2 mb-lg-0">' .
    '<li class="nav-item">' .
    '<a class="nav-link active" aria-current="page" href="index.php">Domov</a>' .
    '</li>' .
    '<li class="nav-item">' .
    '<a class="nav-link" href="izpisPsov.php">Seznam psov</a>' .
    '</li>' .
    '<li class="nav-item">' .
    '<a class="nav-link" href="izpisPoti.php">Seznam poti</a>' .
    '</li>' .
    '</ul>' .
    '<form class="d-flex" action="registracija.php">' .
    '<button class="btn btn-outline-success">Registracija</button>' .
    '</form>' .
    '<form class="d-flex ms-2" action="prijava.php">' .
    '<button class="btn btn-outline-primary">Prijava</button>' .
    '</form>' .
    '</div>' .
    '</div>';
}

?>