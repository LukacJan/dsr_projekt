<?php
mb_internal_encoding("UTF-8");
include_once("povezava.php");
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    //dobi podatke poti vnesene v form preko post metode
    echo "<br />";
    echo $_POST['naziv'];
    echo $_POST['dolzina'];
    echo $_POST['trajanje'];
    echo $_POST['podlaga'];
    echo $_POST['lokacijaVnos'];
    echo $_POST['naziv'];
    echo $_POST['naziv'];
    echo $_POST['naziv'];
    $naziv = $_POST['naziv'];
    $dolzina = $_POST['dolzina'];
    $trajanje = $_POST['trajanje'];
    $uporabnikID = $_SESSION['prijavljen_id'];
    $podlaga = $_POST['podlaga'];
    $lokacijaVnos = $_POST['lokacijaVnos'];
    //$_POST['lokacija'];
    $lokacijaVnosKon = $_POST['lokacijaVnosKon'];
    //$_POST['lokacijaKon'];
    $pes_id = $_POST['pes'];
    echo "<br />";
    print_r($_FILES);

    try {
        $lokacijaZac;
        $lokacijaKon;
        if ($lokacijaVnos == '') {
            $lokacijaZac = $_POST['lokacija'];
        } else {
            $lokacijaZac = $lokacijaVnos;
            $sql = "INSERT INTO lokacija (kraj) VALUES (?)";
            $stmt = $conn->prepare($sql);
            $stmt->execute([$lokacijaZac]);
        }

        if ($lokacijaVnosKon == '') {
            $lokacijaKon = $_POST['lokacijaKon'];
            echo "<br/>";
            echo $_POST['lokacijaKon'];
            echo "<br/>";
        } else {
            $lokacijaKon = $lokacijaVnosKon;
            $sql = "INSERT INTO lokacija (kraj) VALUES (?)";
            $stmt = $conn->prepare($sql);
            $stmt->execute([$lokacijaKon]);
        }

        $sql = "INSERT INTO pot (naziv, dolzina, trajanje, TK_ID_uporabnik, TK_ID_podlaga, TK_ID_pes) VALUES (?,?,?,?,?,?)";
        $stmt = $conn->prepare($sql);
        $stmt->execute([$naziv, $dolzina, $trajanje, $uporabnikID, $podlaga, $pes_id]);
        $id = $conn->lastInsertId();
        echo $id;


        if ($lokacijaVnos != '') {
            $stmt3 = $conn->prepare('SELECT id_lokacija  FROM lokacija WHERE kraj = ? ');
            $stmt3->execute(array($lokacijaZac));
            $kraj_zac = $stmt3->fetchColumn();

            $sql = "INSERT INTO pot_has_lokacija (zac, kon, TK_ID_pot, TK_ID_lokacija) VALUES (?,?,?,?)";
            $stmt = $conn->prepare($sql);
            $stmt->execute([1, 0, $id, $kraj_zac]);
        } else {
            $sql = "INSERT INTO pot_has_lokacija (zac, kon, TK_ID_pot, TK_ID_lokacija) VALUES (?,?,?,?)";
            $stmt = $conn->prepare($sql);
            $stmt->execute([1, 0, $id, $lokacijaZac]);
        }



        if ($lokacijaVnosKon != '') {
            $stmt3 = $conn->prepare('SELECT id_lokacija  FROM lokacija WHERE kraj = ? ');
            $stmt3->execute(array($lokacijaKon));
            $kraj_kon = $stmt3->fetchColumn();

            $sql = "INSERT INTO pot_has_lokacija (zac, kon, TK_ID_pot, TK_ID_lokacija) VALUES (?,?,?,?)";
            $stmt = $conn->prepare($sql);
            $stmt->execute([0, 1, $id, $kraj_kon]);
        } else {
            $sql = "INSERT INTO pot_has_lokacija (zac, kon, TK_ID_pot, TK_ID_lokacija) VALUES (?,?,?,?)";
            $stmt = $conn->prepare($sql);
            $stmt->execute([0, 1, $id, $lokacijaKon]);
        }

        if (file_exists($_FILES['files']['tmp_name']) || is_uploaded_file($_FILES['files']['tmp_name'])) {

            $target_dir = "slike/";
            $temp = explode(".", $_FILES["files"]["name"]);
            $newfilename = round(microtime(true)) . '.' . end($temp);

            $target_file = $target_dir . $newfilename;
            $uploadOk = 1;
            $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

            if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
                echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $uploadOk = 0;
            }

            if ($uploadOk == 0) {
                echo "Sorry, your file was not uploaded.";
                // if everything is ok, try to upload file
            } else {
                if (move_uploaded_file($_FILES["files"]["tmp_name"], $target_file)) {

                    $sql2 = "INSERT INTO slika (naziv, TK_ID_pot) VALUES (?,?)";
                    $stmt2 = $conn->prepare($sql2);
                    $stmt2->execute([$newfilename, $id]);
                    echo "The file " . htmlspecialchars(basename($_FILES["files"]["name"])) . " has been uploaded.";

                    echo "<br />";
                    echo $newfilename;
                    echo "<br />";
                    echo $newfilename;
                } else {
                    echo "Sorry, there was an error uploading your file.";
                }
            }
        }




        echo "<script>console.log('New record created successfully');</script>";
        echo '<div class="alert alert-success alert-dismissible fade show" role="alert">
               Vnesena je bil nova pot.
               <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>';
        //header("Location: index.php");
    } catch (PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
        echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
               Pri vnosu je prišlo do napake.
               <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>';
    }
}
