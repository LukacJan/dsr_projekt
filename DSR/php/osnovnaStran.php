<?php
    if (isset($_SESSION["prijavljen_id"])) {
        //echo $_SESSION["prijavljen_id"];
        //echo "<br/>";
        //print_r($_SESSION);
        //include("php/odjava.php");
    }
    $ip;
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }

    if ($ip == "::1") {
        $location = file_get_contents('http://api.ipstack.com/164.8.243.222?access_key=0615d7aff3db9f45f605446f95a33593&format=1');
        $obj =json_decode($location);
        //print $obj->{'city'};
        $city = $obj->{'city'};
    }else {
    $location = file_get_contents('http://api.ipstack.com/' . $ip .'?access_key=0615d7aff3db9f45f605446f95a33593&format=1');
    $obj =json_decode($location);
    //print $obj->{'city'};
    $city = $obj->{'city'};
    }
    echo '<iframe src="https://vreme.arso.gov.si/widget/?&loc='. $city .'" style="border:0; height: 185px; width: 100%;"></iframe>';

    $vreme = file_get_contents('https://api.openweathermap.org/data/2.5/weather?q='. $city .'&appid=6b919022ce964ddd09e483b14a614d69&units=metric');
    //print $vreme;
    
    //var_dump($vreme);
    $vreme = json_decode($vreme);
    //var_dump($vreme);
    //print $vreme;

    //echo $vreme->main->temp;
    //echo "<br/>";
    //echo "<br/>";
    //var_dump($vreme->weather[0]->main);
    //echo "<br/>";
    //echo "<br/>";

    if ($vreme->main->temp < -5.0) {
        echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
        <h4 class="alert-heading">Sprehod zaradi nizkih temperatur ni priporočljiv!</h4>
        <p>Zaradi tega, ker je v vaši približni okolici '. $vreme->main->temp .'°C, vam sprehoda ne priporočamo.</p>
        <hr>
        <p class="mb-0">Glejte napoved, če bo v prihodnjih urah temperatura kaj bolj primerna.</p>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>';
    }elseif ($vreme->main->feels_like < -6.0) {
        echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
        <h4 class="alert-heading">Sprehod zaradi nizkih temperatur ni priporočljiv!</h4>
        <p>Zaradi tega, ker je v vaši približni okolici občutek temperature '. $vreme->main->feels_like .'°C, vam sprehoda ne priporočamo.</p>
        <hr>
        <p class="mb-0">Glejte napoved, če bo v prihodnjih urah temperatura kaj bolj primerna.</p>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>';
    }elseif ($vreme->weather[0]->main == "Thunderstorm") {
        echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
        <h4 class="alert-heading">Sprehod zaradi grmljenja, bliskanja in nevihte ni priporočljiv!</h4>
        <p>Zaradi tega, ker je v vaši približni okolici trenutno nevihta, vam sprehoda ne priporočamo.</p>
        <hr>
        <p class="mb-0">Sprehod vam priporočamo, ko nevihta premine. Ne pozabite gledati vremensko napoved</p>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>';
    }elseif ($vreme->weather[0]->main == "Rain") {
        echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
        <h4 class="alert-heading">Sprehod zaradi dežja ni priporočljiv.</h4>
        <p>Zaradi tega, ker je v vaši približni okolici pada dež, vam sprehoda ne priporočamo.</p>
        <hr>
        <p class="mb-0">Sprehod vam priporočamo, ko dež premine. Ne pozabite gledati vremensko napoved</p>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>';
    }
    elseif ($vreme->main->temp > 28.0) {
        echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
        <h4 class="alert-heading">Sprehod zaradi visokih temperatur ni priporočljiv!</h4>
        <p>Zaradi tega, ker je v vaši približni okolici temperatura '. $vreme->main->temp .'°C vam sprehoda ne priporočamo.</p>
        <hr>
        <p class="mb-0">Sprehod vam priporočamo, ko temperatura pade. Ne pozabite gledati vremensko napoved.</p>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>';
    }elseif ($vreme->main->feels_like > 30.0) {
        echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
        <h4 class="alert-heading">Sprehod zaradi visokih temperatur ni priporočljiv!</h4>
        <p>Zaradi tega, ker je v vaši približni okolici občutek temperature '. $vreme->main->feels_like .'°C vam sprehoda ne priporočamo.</p>
        <hr>
        <p class="mb-0">Sprehod vam priporočamo, ko temperatura pade. Ne pozabite gledati vremensko napoved.</p>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>';
    }elseif ($vreme->wind->speed > 10.0 && $vreme->main->temp < 5.0) {
        echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
        <h4 class="alert-heading">Sprehod zaradi nižjih temperatur in hitrosti vetra ni priporočljiv!</h4>
        <p>Zaradi tega, ker je v vaši približni okolici temperatura '. $vreme->main->temp .'°C in hitrost vetra '. $vreme->wind->speed .'m/s, vam sprehoda ne priporočamo.</p>
        <hr>
        <p class="mb-0">Sprehod vam priporočamo, ko veter poneha. Ne pozabite gledati vremensko napoved.</p>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>';
    }elseif ($vreme->wind->speed > 15.0) {
        echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
        <h4 class="alert-heading">Sprehod zaradi hitrosti vetra ni priporočljiv!</h4>
        <p>Zaradi tega, ker je v vaši približni okolici hitrost vetra '. $vreme->wind->speed .'m/s, vam sprehoda ne priporočamo.</p>
        <hr>
        <p class="mb-0">Sprehod vam priporočamo, ko veter poneha. Ne pozabite gledati vremensko napoved.</p>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>';
    }elseif ($vreme->weather[0]->main == "Drizzle") {
        if ($vreme->weather[0]->description == "light intensity drizzle"  || $vreme->weather[0]->description == "drizzle") {
            echo '<div class="alert alert-success alert-dismissible fade show" role="alert">
            <h4 class="alert-heading">Kljub šibkemu rosenju, vam sprehod priporočamo.</h4>
            <p>Sprehod vam priporočamo, ker ni močnih padavin.</p>
            <hr>
            <p class="mb-0">Sprehod vam priporočamo, dokler se padavine ne ojačajo. Ne pozabite gledati vremensko napoved.</p>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>';
        } else {
            echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
            <h4 class="alert-heading">Sprehod zaradi padavin ni priporočljiv!</h4>
            <p>Zaradi tega, ker je v vaši približni okolici padajo padavine, vam sprehoda ne priporočamo.</p>
            <hr>
            <p class="mb-0">Sprehod vam priporočamo, ko padavine ponehajo. Ne pozabite gledati vremensko napoved.</p>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>';
        }
        
    }elseif ($vreme->weather[0]->main == "Snow") {
        echo '<div class="alert alert-success alert-dismissible fade show" role="alert">
        <h4 class="alert-heading">Kljub snežeju, vam sprehod priporočamo.</h4>
        <hr>
        <p class="mb-0">Sprehod vam priporočamo, dokler se sneg ne spremeni v dež. Ne pozabite gledati vremensko napoved.</p>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>';
    }elseif ($vreme->weather[0]->main == "Smoke" || $vreme->weather[0]->main == "Dust" || $vreme->weather[0]->main == "Ash") {
        echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
        <h4 class="alert-heading">Sprehod zaradi slabega ali onesnaženega zraka ni priporočljiv!</h4>
        <p>Zaradi tega, ker je v vaši približni okolici zrak preveč slab ali resno onesažen, vam sprehoda ne priporočamo.</p>
        <hr>
        <p class="mb-0">Sprehod vam priporočamo, ko se kakovost zraka izboljša. Ne pozabite gledati vremensko napoved.</p>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>';
    }elseif ($vreme->weather[0]->main == "Tornado") {
        echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
        <h4 class="alert-heading">Torndo!!!!!! Ne na sprehod!!!!!! Nikamor ven!!!!</h4>
        <p>Bodite na varnem!!!</p>
        <hr>
        <p class="mb-0">Sprehod vam ne priporočamo!!!! Ne pozabite gledati vremensko napoved.</p>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>';
    }elseif ($vreme->weather[0]->main == "Squall") {
        echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
        <h4 class="alert-heading">Sprehod vam trenutno ni priporočljiv!</h4>
        <p>Zaradi čudnega obnašanja vremena, vam sprehod ne priporočamo.</p>
        <hr>
        <p class="mb-0">Sprehod vam ne priporočamo! Ne pozabite gledati vremensko napoved.</p>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>';
    }else {
        echo '<div class="alert alert-success alert-dismissible fade show" role="alert">
        <h4 class="alert-heading">Vreme je trenutno primerno za sprehod.</h4>
        <hr>
        <p class="mb-0">Sprehod vam priporočamo, dokler se vreme ne spremeni. Ne pozabite gledati vremensko napoved.</p>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>';
    }
?>