<?php
mb_internal_encoding("UTF-8");
include_once("povezava.php");

if ($_SERVER["REQUEST_METHOD"] == "POST") {
   // username and password sent from form 


   //dobi email in geslo iz forme poslano po post metodi
   $email = $_POST['email'];
   $geslo = $_POST['geslo'];

   //dobimo sol uporabnika z tem mailom
   $prvo = $conn->prepare('SELECT sol  FROM uporabnik WHERE email = ?');
   $prvo->execute(array($email));
   $sol = $prvo->fetchColumn();

   //heširamo vpisano geslo skupaj s soljo
   $hashGeslo = hash('ripemd160', $geslo . $sol);
   /*
   echo "test";
   echo "<br />";
   echo $sol;
   echo "<br />";
   echo $hashGeslo;
   echo "<br />";*/
   echo "<br />";

   //dobimo id uporabnika ki ima enak mail, kot je vnesen in geslo enako heširanemu s soljo
   $stmt = $conn->prepare('SELECT id_uporabnik  FROM uporabnik WHERE email = ? AND geslo = ?');
   $stmt->execute(array($email, $hashGeslo));
   $odg = $stmt->fetchColumn();
   //print_r($odg);
   //echo $odg;

   $kontrola = -1;

   if ($odg != '') {
      $_SESSION['prijavljen_id'] = $odg;
      //echo $_SESSION['prijavljen_id'];
      $kontrola = 100;
   }

   if ($kontrola == -1) {
      echo '<div class="alert alert-danger" role="alert">
               Vnesli ste napačne podatke. Prosimo vas, da jih vnesete pravilno.
            </div>';
   } else {
      header("Location: index.php");
      exit();
   }

   /*

    $active = $row['active'];
    
    $count = mysqli_num_rows($result);
    
    // If result matched $myusername and $mypassword, table row must be 1 row
      
    if($count == 1) {
       session_register("myusername");
       $_SESSION['login_user'] = $myusername;
       
       header("location: welcome.php");
    }else {
       $error = "Your Login Name or Password is invalid";
    }*/
}
