<?php

?>
<?php include 'php/povezava.php'; ?>
<html>

<head>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" />
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <!-- <script src="script/nav_bar.js"></script> -->
</head>

<body>
    <nav id="nav" class="navbar navbar-expand-lg navbar-light bg-light">
        <?php
        include("php/nav_bar.php");
        ?>
    </nav>

    <form method="post" onsubmit="" enctype="multipart/form-data">
        <div class="form-group">
            <label for="inputIme">Ime</label>
            <input type="text" class="form-control" id="inputIme" name="ime" placeholder="Vnesi ime">
        </div>
        <br />
        <div class="form-group">
            <label for="inputTeza">Teža</label>
            <input type="number" min="0" max="99" class="form-control" id="inputTeza" name="teza">
        </div>
        <br />
        <div class="form-group">
            <label for="inputLeto">Leto rojstva</label>
            <input type="number" min="2000" max="2030" class="form-control" id="inputLeto" name="leto">
        </div>
        <br />
        <div class="form-group">
            Spol: <br />
            <input type="radio" class="form-check-input" id="m" name="spol" value="1">
            <label for="m" class="form-check-label">Moški</label><br>
            <input type="radio" class="form-check-input" id="z" name="spol" value="2">
            <label for="z" class="form-check-label">Ženska</label><br>
        </div>
        <br />
        <div class="form-group">
            Aktivnost psa: <br />
            <input type="radio" class="form-check-input" id="ne" name="aktivnost" value="1">
            <label for="ne" class="form-check-label">Ne aktiven</label><br>
            <input type="radio" class="form-check-input" id="malo" name="aktivnost" value="2">
            <label for="malo" class="form-check-label">Malo aktiven</label><br>
            <input type="radio" class="form-check-input" id="zmerno" name="aktivnost" value="3">
            <label for="zmerno" class="form-check-label">Zmerno aktiven</label><br>
            <input type="radio" class="form-check-input" id="precej" name="aktivnost" value="4">
            <label for="precej" class="form-check-label">Precej aktiven</label><br>
            <input type="radio" class="form-check-input" id="ekstremno" name="aktivnost" value="5">
            <label for="ekstremno" class="form-check-label">Ekstremno aktiven</label><br>
        </div>
        <br />

        <?php
        mb_internal_encoding("UTF-8");
        include_once("php/povezava.php");

        $prvo = $conn->prepare('SELECT * FROM pasma');
        $prvo->execute();
        $result = $prvo->fetchAll(PDO::FETCH_ASSOC);

        echo '<div class="form-group"> Pasma: <br/>
            <select class="form-select" name="pasma" aria-label="Default select example">';
        for ($i = 0; $i < count($result); $i++) {
            $id = $result[$i]['id_pasma'];
            $string = '<option value="' . $result[$i]["id_pasma"] . '">' . $result[$i]["naziv"] . '</option>';
            echo $string;
        }
        echo '</select> </div> <br/>';

        //print_r($result);
        ?>

        <div class="form-group">
            <label for="files" class="form-label">Multiple files input example</label>
            <input class="form-control" name="files" type="file" id="files" multiple>
        </div>
        <br />

        <button type="submit" class="btn btn-primary">Vnesi</button>
    </form>
    <?php include 'php/vnosPsa.php'; ?>
</body>

</html>