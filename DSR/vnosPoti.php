<?php

?>
<?php include 'php/povezava.php'; ?>
<html>

<head>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" />
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <!-- <script src="script/nav_bar.js"></script> -->
</head>

<body>
    <nav id="nav" class="navbar navbar-expand-lg navbar-light bg-light">
        <?php
        include("php/nav_bar.php");
        ?>
    </nav>

    <form method="post" onsubmit="" enctype="multipart/form-data">
        <div class="form-group">
            <label for="inputNaziv">Naziv poti</label>
            <input type="text" class="form-control" id="inputNaziv" name="naziv" placeholder="Vnesi naziv">
        </div>
        <br />
        <div class="form-group">
            <label for="inputDolzina">Dolžina poti (v metrih)</label>
            <input type="number" min="0" max="99999" class="form-control" id="inputDolzina" name="dolzina">
        </div>
        <br />
        <div class="form-group">
            <label for="inputTrajanje">Trajanje (v minutah)</label>
            <input type="number" min="0" max="10000" class="form-control" id="inputTrajanje" name="trajanje">
        </div>
        <br />

        <?php
        mb_internal_encoding("UTF-8");
        include_once("php/povezava.php");

        $prvo = $conn->prepare('SELECT * FROM podlaga');
        $prvo->execute();
        $result = $prvo->fetchAll(PDO::FETCH_ASSOC);

        echo '<div class="form-group"> Podlaga: <br/>
            <select class="form-select" name="podlaga" aria-label="Default select example">';

        for ($i = 0; $i < count($result); $i++) {
            $id = $result[$i]['id_podlaga'];
            $string = '<option value="' . $result[$i]["id_podlaga"] . '">' . $result[$i]["naziv"] . '</option>';
            echo $string;
        }
        echo '</select> </div> <br/>';

        ?>

        <div class="form-group">
            <label for="inputLokacijaVnos">Izberite ali vnesite začetno lokacijo</label>
            <input type="text" class="form-control" id="inputLokacijaVnos" name="lokacijaVnos" placeholder="Vnesi lokacijo, le če je še ni na voljo v izbiri">
        </div>

        <?php
        mb_internal_encoding("UTF-8");
        include_once("php/povezava.php");

        $prvo = $conn->prepare('SELECT * FROM lokacija');
        $prvo->execute();
        $result = $prvo->fetchAll(PDO::FETCH_ASSOC);

        echo '<div class="form-group"> Lokacija: <br/>
            <select class="form-select" name="lokacija" aria-label="Default select example">';

        for ($i = 0; $i < count($result); $i++) {
            $id = $result[$i]['id_lokacija'];
            $string = '<option value="' . $result[$i]["id_lokacija"] . '">' . $result[$i]["kraj"] . '</option>';
            echo $string;
        }
        echo '</select> </div> <br/>';

        ?>

        <div class="form-group">
            <label for="inputLokacijaVnosKon">Izberite ali vnesite končno lokacijo</label>
            <input type="text" class="form-control" id="inputLokacijaVnosKon" name="lokacijaVnosKon" placeholder="Vnesi lokacijo, le če je še ni na voljo v izbiri">
        </div>

        <?php
        mb_internal_encoding("UTF-8");
        include_once("php/povezava.php");

        $prvo = $conn->prepare('SELECT * FROM lokacija');
        $prvo->execute();
        $result = $prvo->fetchAll(PDO::FETCH_ASSOC);

        echo '<div class="form-group"> Lokacija: <br/>
            <select class="form-select" name="lokacijaKon" aria-label="Default select example">';

        for ($i = 0; $i < count($result); $i++) {
            $id = $result[$i]['id_lokacija'];
            $string = '<option value="' . $result[$i]["id_lokacija"] . '">' . $result[$i]["kraj"] . '</option>';
            echo $string;
        }
        echo '</select> </div> <br/>';

        ?>



        <?php
        mb_internal_encoding("UTF-8");
        include_once("php/povezava.php");

        $prvo = $conn->prepare('SELECT * FROM pes WHERE TK_ID_uporabnik = ?');
        $prvo->execute(array($_SESSION['prijavljen_id']));
        $result = $prvo->fetchAll(PDO::FETCH_ASSOC);

        echo '<div class="form-group"> Pes: <br/>
            <select class="form-select" name="pes" aria-label="Default select example">';

        for ($i = 0; $i < count($result); $i++) {
            $id = $result[$i]['id_pes'];
            $string = '<option value="' . $result[$i]["id_pes"] . '">' . $result[$i]["ime"] . '</option>';
            echo $string;
        }
        echo '</select> </div> <br/>';

        ?>



        <div class="form-group">
            <label for="files" class="form-label">Izberi datoteko s sliko</label>
            <input class="form-control" name="files" type="file" id="files" multiple>
        </div>
        <br />

        <button type="submit" class="btn btn-primary">Vnesi</button>
    </form>
    <?php include 'php/vnosPoti.php'; ?>
</body>

</html>