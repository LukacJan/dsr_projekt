-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 29, 2021 at 06:02 PM
-- Server version: 5.7.31
-- PHP Version: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `stran_za_lastnike_psov`
--

-- --------------------------------------------------------

--
-- Table structure for table `aktivnost`
--

DROP TABLE IF EXISTS `aktivnost`;
CREATE TABLE IF NOT EXISTS `aktivnost` (
  `id_aktivnost` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(20) COLLATE utf8mb4_slovenian_ci NOT NULL,
  `stopnja` int(11) NOT NULL,
  PRIMARY KEY (`id_aktivnost`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_slovenian_ci;

--
-- Dumping data for table `aktivnost`
--

INSERT INTO `aktivnost` (`id_aktivnost`, `naziv`, `stopnja`) VALUES
(1, 'ne aktiven', 0),
(2, 'malo aktiven', 1),
(3, 'zmerno aktiven', 2),
(4, 'precej aktiven', 3),
(5, 'ekstremno aktiven', 4);

-- --------------------------------------------------------

--
-- Table structure for table `lokacija`
--

DROP TABLE IF EXISTS `lokacija`;
CREATE TABLE IF NOT EXISTS `lokacija` (
  `id_lokacija` int(11) NOT NULL AUTO_INCREMENT,
  `kraj` varchar(40) COLLATE utf8mb4_slovenian_ci NOT NULL,
  PRIMARY KEY (`id_lokacija`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_slovenian_ci;

--
-- Dumping data for table `lokacija`
--

INSERT INTO `lokacija` (`id_lokacija`, `kraj`) VALUES
(1, 'Maribor'),
(2, 'Maribor');

-- --------------------------------------------------------

--
-- Table structure for table `ocena`
--

DROP TABLE IF EXISTS `ocena`;
CREATE TABLE IF NOT EXISTS `ocena` (
  `id_ocena` int(11) NOT NULL AUTO_INCREMENT,
  `ocena` int(11) NOT NULL,
  `komentar` varchar(300) COLLATE utf8mb4_slovenian_ci DEFAULT NULL,
  `TK_ID_pot` int(11) NOT NULL,
  `TK_ID_uporabnik` int(11) NOT NULL,
  PRIMARY KEY (`id_ocena`),
  KEY `TK_ID_pot` (`TK_ID_pot`),
  KEY `TK_ID_uporabnik` (`TK_ID_uporabnik`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_slovenian_ci;

--
-- Dumping data for table `ocena`
--

INSERT INTO `ocena` (`id_ocena`, `ocena`, `komentar`, `TK_ID_pot`, `TK_ID_uporabnik`) VALUES
(1, 5, NULL, 1, 1),
(2, 3, NULL, 1, 4),
(3, 5, 'Nek komentar', 1, 17),
(4, 2, 'Test', 1, 17);

-- --------------------------------------------------------

--
-- Table structure for table `pasma`
--

DROP TABLE IF EXISTS `pasma`;
CREATE TABLE IF NOT EXISTS `pasma` (
  `id_pasma` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(40) COLLATE utf8mb4_slovenian_ci NOT NULL,
  PRIMARY KEY (`id_pasma`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_slovenian_ci;

--
-- Dumping data for table `pasma`
--

INSERT INTO `pasma` (`id_pasma`, `naziv`) VALUES
(1, 'Nemški ovčar'),
(2, 'Labradorec'),
(3, 'Zlati prinašalec');

-- --------------------------------------------------------

--
-- Table structure for table `pes`
--

DROP TABLE IF EXISTS `pes`;
CREATE TABLE IF NOT EXISTS `pes` (
  `id_pes` int(11) NOT NULL AUTO_INCREMENT,
  `ime` varchar(50) COLLATE utf8mb4_slovenian_ci NOT NULL,
  `teza` decimal(10,2) NOT NULL,
  `leto_rojstva` year(4) NOT NULL,
  `TK_ID_uporabnik` int(11) DEFAULT NULL,
  `TK_ID_spol` int(11) NOT NULL,
  `TK_ID_aktivnost` int(11) NOT NULL,
  `TK_ID_pasma` int(11) NOT NULL,
  PRIMARY KEY (`id_pes`),
  KEY `TK_ID_spol` (`TK_ID_spol`),
  KEY `TK_ID_pasma` (`TK_ID_pasma`),
  KEY `TK_ID_aktivnost` (`TK_ID_aktivnost`),
  KEY `pes_ibfk_1` (`TK_ID_uporabnik`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_slovenian_ci;

--
-- Dumping data for table `pes`
--

INSERT INTO `pes` (`id_pes`, `ime`, `teza`, `leto_rojstva`, `TK_ID_uporabnik`, `TK_ID_spol`, `TK_ID_aktivnost`, `TK_ID_pasma`) VALUES
(1, 'Bobi', '32.00', 2006, 17, 1, 2, 3),
(2, 'Fidella', '28.00', 2019, 17, 2, 3, 1),
(3, 'Pes', '40.00', 2000, 17, 1, 5, 2),
(4, 'Test', '20.00', 2000, 17, 2, 3, 1),
(6, 'Fidella', '30.00', 2019, 17, 2, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pes_has_zivljenjsko_stanje`
--

DROP TABLE IF EXISTS `pes_has_zivljenjsko_stanje`;
CREATE TABLE IF NOT EXISTS `pes_has_zivljenjsko_stanje` (
  `id_pes_has_zivljenjsko_stanje` int(11) NOT NULL AUTO_INCREMENT,
  `date_od` date NOT NULL,
  `date_do` date NOT NULL,
  `TK_ID_pes` int(11) NOT NULL,
  `TK_ID_zivljenjsko_stanje` int(11) NOT NULL,
  PRIMARY KEY (`id_pes_has_zivljenjsko_stanje`),
  KEY `TK_ID_pes` (`TK_ID_pes`),
  KEY `TK_ID_zivljenjsko_stanje` (`TK_ID_zivljenjsko_stanje`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_slovenian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `podlaga`
--

DROP TABLE IF EXISTS `podlaga`;
CREATE TABLE IF NOT EXISTS `podlaga` (
  `id_podlaga` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(50) COLLATE utf8mb4_slovenian_ci NOT NULL,
  PRIMARY KEY (`id_podlaga`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_slovenian_ci;

--
-- Dumping data for table `podlaga`
--

INSERT INTO `podlaga` (`id_podlaga`, `naziv`) VALUES
(1, 'asfalt'),
(2, 'šoder'),
(3, 'beton'),
(4, 'trava'),
(5, 'sneg'),
(6, 'zemlja'),
(7, 'blato'),
(8, 'skala'),
(9, 'mešana površina');

-- --------------------------------------------------------

--
-- Table structure for table `pot`
--

DROP TABLE IF EXISTS `pot`;
CREATE TABLE IF NOT EXISTS `pot` (
  `id_pot` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(60) COLLATE utf8mb4_slovenian_ci NOT NULL,
  `dolzina` decimal(10,2) NOT NULL,
  `trajanje` int(11) NOT NULL,
  `TK_ID_uporabnik` int(11) NOT NULL,
  `TK_ID_podlaga` int(11) NOT NULL,
  `TK_ID_pes` int(11) NOT NULL,
  PRIMARY KEY (`id_pot`),
  KEY `TK_ID_uporabnik` (`TK_ID_uporabnik`),
  KEY `fk_id_podlaga` (`TK_ID_podlaga`),
  KEY `fk_id_pes_fk` (`TK_ID_pes`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_slovenian_ci;

--
-- Dumping data for table `pot`
--

INSERT INTO `pot` (`id_pot`, `naziv`, `dolzina`, `trajanje`, `TK_ID_uporabnik`, `TK_ID_podlaga`, `TK_ID_pes`) VALUES
(1, 'Testen sprehod', '1000.00', 60, 17, 1, 1),
(2, 'Test slika', '123.00', 123, 17, 1, 1),
(3, 'Testen sprehod', '123.00', 123, 17, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pot_has_lokacija`
--

DROP TABLE IF EXISTS `pot_has_lokacija`;
CREATE TABLE IF NOT EXISTS `pot_has_lokacija` (
  `id_pot_has_lokacija` int(11) NOT NULL AUTO_INCREMENT,
  `zac` tinyint(1) NOT NULL,
  `kon` tinyint(1) NOT NULL,
  `TK_ID_pot` int(11) NOT NULL,
  `TK_ID_lokacija` int(11) NOT NULL,
  PRIMARY KEY (`id_pot_has_lokacija`),
  KEY `TK_ID_lokacija` (`TK_ID_lokacija`),
  KEY `TK_ID_pot` (`TK_ID_pot`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_slovenian_ci;

--
-- Dumping data for table `pot_has_lokacija`
--

INSERT INTO `pot_has_lokacija` (`id_pot_has_lokacija`, `zac`, `kon`, `TK_ID_pot`, `TK_ID_lokacija`) VALUES
(1, 1, 0, 1, 1),
(2, 0, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pot_has_pes`
--

DROP TABLE IF EXISTS `pot_has_pes`;
CREATE TABLE IF NOT EXISTS `pot_has_pes` (
  `id_pot_has_pes` int(11) NOT NULL AUTO_INCREMENT,
  `TK_ID_pes` int(11) NOT NULL,
  `TK_ID_pot` int(11) NOT NULL,
  PRIMARY KEY (`id_pot_has_pes`),
  KEY `fk_id_pes_fk` (`TK_ID_pes`),
  KEY `fk_id_pot_fk` (`TK_ID_pot`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_slovenian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `slika`
--

DROP TABLE IF EXISTS `slika`;
CREATE TABLE IF NOT EXISTS `slika` (
  `id_slika` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `TK_ID_pes` int(11) DEFAULT NULL,
  `TK_ID_pot` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_slika`),
  KEY `fk_id_pes` (`TK_ID_pes`),
  KEY `fk_id_pot` (`TK_ID_pot`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_slovenian_ci;

--
-- Dumping data for table `slika`
--

INSERT INTO `slika` (`id_slika`, `naziv`, `TK_ID_pes`, `TK_ID_pot`) VALUES
(3, '1640534750.jpg', 6, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `spol`
--

DROP TABLE IF EXISTS `spol`;
CREATE TABLE IF NOT EXISTS `spol` (
  `id_spol` int(11) NOT NULL AUTO_INCREMENT,
  `spol` varchar(20) COLLATE utf8mb4_slovenian_ci NOT NULL,
  PRIMARY KEY (`id_spol`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_slovenian_ci;

--
-- Dumping data for table `spol`
--

INSERT INTO `spol` (`id_spol`, `spol`) VALUES
(1, 'moški'),
(2, 'ženska');

-- --------------------------------------------------------

--
-- Table structure for table `uporabnik`
--

DROP TABLE IF EXISTS `uporabnik`;
CREATE TABLE IF NOT EXISTS `uporabnik` (
  `id_uporabnik` int(11) NOT NULL AUTO_INCREMENT,
  `ime` varchar(40) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `priimek` varchar(50) COLLATE utf8mb4_slovenian_ci NOT NULL,
  `email` varchar(60) COLLATE utf8mb4_slovenian_ci NOT NULL,
  `sol` int(10) NOT NULL,
  `geslo` varchar(70) COLLATE utf8mb4_slovenian_ci NOT NULL,
  `TK_ID_vloga` int(11) NOT NULL,
  `TK_ID_spol` int(11) NOT NULL,
  PRIMARY KEY (`id_uporabnik`),
  UNIQUE KEY `email` (`email`),
  KEY `TK_ID_vloga` (`TK_ID_vloga`),
  KEY `TK_ID_spol` (`TK_ID_spol`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_slovenian_ci;

--
-- Dumping data for table `uporabnik`
--

INSERT INTO `uporabnik` (`id_uporabnik`, `ime`, `priimek`, `email`, `sol`, `geslo`, `TK_ID_vloga`, `TK_ID_spol`) VALUES
(1, 'Jan', 'Lukač', 'nekaj@nekaj.si', 123, '19e773eb835e0090f313c06407c0c67f273b0b90', 1, 1),
(2, 'jan', 'lukač', 'jan.lukac@gmail.com', 123, 'hashgeslo', 1, 1),
(3, 'jan', 'lukaÄ', 'janlukac2000@gmail.com', 15033493, '2e5c8eb9d42b58cc8f3e65bf7cd70c8786bdd70e', 1, 1),
(4, 'Jan', 'LukaÄ', 'janlukac2000@gl.com', 75792917, '91343abb297467535e56a687e817b3a056783fb6', 1, 1),
(5, 'Jan', 'LukaÄ', 'janlukac2000@gail.com', 22314128, '3cee8efae58674d5b49ce3e391d4348205350f4c', 1, 1),
(6, 'Jan', 'lukaÄ', 'janlukac@gmail.com', 43896072, '9f3f4b6d110d03bda92da07b677a038864c328ce', 1, 1),
(7, 'Jan', 'LukaÄ', 'janlukac2000@a.com', 11934639, '7a326f64b188cef86c5bb2d9364cd2ad35bec6f3', 1, 1),
(8, 'test', 'Å¡ÄÄ‡Ä‘Å¾', 'neak@bla.c', 92655745, '8165dff1879ca8cee790891973f587cf2c904f26', 1, 2),
(9, 'test', 'teÅ¡ÄÄ‘t123', '123@test.d', 42757463, 'df07890ad40daeb18cf29d334888f5f5dc1bbc09', 1, 2),
(10, 'Ã…Â¡Ã„Â‘Ã„ÂÃ„Â‡Ã…Â¾', 'Ã…Â¡Ã„Â‘Ã…Â¾Ã„ÂÃ„Â‡', 'test@sumniki.si', 59441032, '9e23299001b2386414459bf0f8d5308f0b29363e', 1, 1),
(11, 'Å¾ÄÅ¡ÄÄ', 'Å¾ÄÄÅ¡ÄÄ', 'nekaj@test.sumnikov', 82383090, 'c1982e9a111fadb4b1519c07b3c6447d1d2c1cd6', 1, 2),
(12, 'ÄÅ¡Å¾ÄÄ', 'ÄÅ¡Å¾ÄÄ', 'nekaj@blah.yes', 71047519, '9eb7741ea25c9f403198d52aff8e88417bc979c8', 1, 1),
(13, 'Å¡ÄÅ¾ÄÄ', 'ÄÅ¾Å¡ÄÄ', 'zdajpazares@upamda.dela', 68668733, 'bcccac6f7112b4b416e2d6256dd1dbb2e75022cd', 1, 1),
(14, 'Å¾ÄÅ¡Ä', 'ÄÄÅ¡ÄÅ¾', 'test1@test1.si', 52962406, '785a6e6c0c7e1a34252ea1d3db492d9608c9fb12', 1, 1),
(15, 'ÄÅ¡Å¾ÄÄ', 'ÄÅ¾Å¡ÄÄ', 'test45@nwk.si', 98815015, 'e92f342507868812bc02eb5ad1587d76f8f4609f', 1, 1),
(16, 'đšžćč', 'žđšćč', '3@nekja.si', 23210282, '4177c75f3fabc357532ff8d29678dd46f160c23d', 1, 1),
(17, 'Jan', 'Lukač', 'nekaj@nekaj.com', 7943206, '3f5a0f23f18e4d2fa4ce29cac230cdd4a8ab622a', 1, 1),
(18, 'Test', 'Test', 'test@test.test.test', 68575238, 'e95ecdb2e55aa04dce76f968cdfc55ca4cd9fb1a', 1, 1),
(20, 'Jan', 'Lukač', 'janlukac1000@gmail.com', 55056232, 'd4766d793c70da62027896a16156b54018f1fba4', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `vloga`
--

DROP TABLE IF EXISTS `vloga`;
CREATE TABLE IF NOT EXISTS `vloga` (
  `id_vloga` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(20) COLLATE utf8mb4_slovenian_ci NOT NULL,
  PRIMARY KEY (`id_vloga`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_slovenian_ci;

--
-- Dumping data for table `vloga`
--

INSERT INTO `vloga` (`id_vloga`, `naziv`) VALUES
(1, 'uporabnik'),
(2, 'administrator');

-- --------------------------------------------------------

--
-- Table structure for table `zivljenjsko_stanje`
--

DROP TABLE IF EXISTS `zivljenjsko_stanje`;
CREATE TABLE IF NOT EXISTS `zivljenjsko_stanje` (
  `id_zivljenjsko_stanje` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(20) COLLATE utf8mb4_slovenian_ci NOT NULL,
  PRIMARY KEY (`id_zivljenjsko_stanje`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_slovenian_ci;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ocena`
--
ALTER TABLE `ocena`
  ADD CONSTRAINT `ocena_ibfk_2` FOREIGN KEY (`TK_ID_pot`) REFERENCES `pot` (`id_pot`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ocena_ibfk_3` FOREIGN KEY (`TK_ID_uporabnik`) REFERENCES `uporabnik` (`id_uporabnik`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pes`
--
ALTER TABLE `pes`
  ADD CONSTRAINT `pes_ibfk_1` FOREIGN KEY (`TK_ID_uporabnik`) REFERENCES `uporabnik` (`id_uporabnik`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `pes_ibfk_2` FOREIGN KEY (`TK_ID_spol`) REFERENCES `spol` (`id_spol`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pes_ibfk_3` FOREIGN KEY (`TK_ID_pasma`) REFERENCES `pasma` (`id_pasma`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pes_ibfk_4` FOREIGN KEY (`TK_ID_aktivnost`) REFERENCES `aktivnost` (`id_aktivnost`) ON UPDATE CASCADE;

--
-- Constraints for table `pes_has_zivljenjsko_stanje`
--
ALTER TABLE `pes_has_zivljenjsko_stanje`
  ADD CONSTRAINT `pes_has_zivljenjsko_stanje_ibfk_1` FOREIGN KEY (`TK_ID_pes`) REFERENCES `pes` (`id_pes`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pes_has_zivljenjsko_stanje_ibfk_2` FOREIGN KEY (`TK_ID_zivljenjsko_stanje`) REFERENCES `zivljenjsko_stanje` (`id_zivljenjsko_stanje`) ON UPDATE CASCADE;

--
-- Constraints for table `pot`
--
ALTER TABLE `pot`
  ADD CONSTRAINT `fk_id_pes_fk` FOREIGN KEY (`TK_ID_pes`) REFERENCES `pes` (`id_pes`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_id_podlaga` FOREIGN KEY (`TK_ID_podlaga`) REFERENCES `podlaga` (`id_podlaga`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pot_ibfk_1` FOREIGN KEY (`TK_ID_uporabnik`) REFERENCES `uporabnik` (`id_uporabnik`);

--
-- Constraints for table `pot_has_lokacija`
--
ALTER TABLE `pot_has_lokacija`
  ADD CONSTRAINT `pot_has_lokacija_ibfk_1` FOREIGN KEY (`TK_ID_lokacija`) REFERENCES `lokacija` (`id_lokacija`),
  ADD CONSTRAINT `pot_has_lokacija_ibfk_2` FOREIGN KEY (`TK_ID_pot`) REFERENCES `pot` (`id_pot`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pot_has_pes`
--
ALTER TABLE `pot_has_pes`
  ADD CONSTRAINT `fk_id_pot_fk` FOREIGN KEY (`TK_ID_pot`) REFERENCES `pot` (`id_pot`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `slika`
--
ALTER TABLE `slika`
  ADD CONSTRAINT `fk_id_pes` FOREIGN KEY (`TK_ID_pes`) REFERENCES `pes` (`id_pes`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_id_pot` FOREIGN KEY (`TK_ID_pot`) REFERENCES `pot` (`id_pot`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `uporabnik`
--
ALTER TABLE `uporabnik`
  ADD CONSTRAINT `uporabnik_ibfk_1` FOREIGN KEY (`TK_ID_vloga`) REFERENCES `vloga` (`id_vloga`) ON UPDATE CASCADE,
  ADD CONSTRAINT `uporabnik_ibfk_2` FOREIGN KEY (`TK_ID_spol`) REFERENCES `spol` (`id_spol`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
